#!/bin/bash

# This script should be run first after a new virtual server is installed.
# It handles the bare minimum to allow you to push in ansible instructions:
# installing required packages and publishing the sshfp DNS records.

hostname="$1"

if [ -z "$hostname" ]; then
  hostname=$(hostname --fqdn)
fi

printf "Using hostname: %s\n" "$hostname"
# Ensure networking is properly setup.

# If we are online already we don't need to mess with networking.
ping -q -c 1 4.2.2.1 1>/dev/null 2>/dev/null

if [ "$?" -eq "0" ]; then
  printf "Networking is already setup.\n"
else
  printf "Networking is not setup.\n"
  # If /etc/systemd/network/ files exist, it means we already have networking 
  # setup.
  network_setup=$(ls /etc/systemd/network)

  if [ -n "$network_setup" ]; then
    printf "Networking is configured by ansible but still not working.\n"
    exit 1
  else
    # The installer configures the network as ens?, but kvm-manager may change it to something else depending on our device list. Make sure the installer network is properly setup.
    links="ens5 ens6 ens7 ens8 ens9 ens10" 
    link=
    for possible_link in $links; do
      ip link show "$possible_link" 1>/dev/null 2>/dev/null
      if [ "$?" -ne "0" ]; then
        continue
      else
        link="$possible_link"
        break
      fi
    done 
    if [ -n "$link" ]; then
      printf "Updating interface name from ens? to %s.\n" "$link"
      sed -i "s/ens[0-9]/${link}/g" /etc/network/interfaces
      ifup "$link" || exit 1 
    else
      printf "I could not figure out which link is active.\n"
      exit 1
    fi
  fi 
fi

# Check for required packages.
printf "\n"
printf " Checking for required packages.\n"

packages="dnsutils python3 wget"
install_packages=

for package in $packages; do
  out=$(dpkg -l "$package" 2>/dev/null | grep "^ii")
  if [ -z "$out" ]; then
    install_packages="$install_packages $package"
  fi
done

if [ -n "$install_packages" ]; then
  printf " Installing %s\n" "$install_packages"
  read -p " Press any key to continue."
  apt install $install_packages
fi


printf " Detected host fqdn: %s\n" "$hostname"

out=$(dig +short -t sshfp "$hostname")

if [ -n "$out" ]; then
  printf " ssh fingerprints already added for this host.\n"
else
  printf " Adding ssh fingerprints via control panel.\n"
  printf " Please enter your control panel username and password below.\n"
  read -p " Username: " cp_username
  read -s -p " Password: " cp_password
  printf "\n"
  
  rsa=$(ssh-keygen -r "$hostname" | grep "SSHFP 1 2" | awk '{print $6}')
  ecdsa=$(ssh-keygen -r "$hostname" | grep "SSHFP 3 2" | awk '{print $6}')
  ed25519=$(ssh-keygen -r "$hostname" | grep "SSHFP 4 2" | awk '{print $6}')
  
  url="https://members.mayfirst.org/cp/api.php"
  hosting_order_id=1

  if echo "$hostname" | grep progressivetech > /dev/null; then
    hosting_order_id=1000002
  fi
  dns_ttl=3600
  for dns_sshfp_algorithm in 1 3 4; do
    if [ "$dns_sshfp_algorithm" -eq "1" ]; then
      dns_sshfp_fpr="$rsa"
    elif [ "$dns_sshfp_algorithm" -eq "3" ]; then
      dns_sshfp_fpr="$ecdsa"
    elif [ "$dns_sshfp_algorithm" -eq "4" ]; then
      dns_sshfp_fpr="$ed25519"
    else
      printf "Not sure what sshfp algorithm that is.\n"
      exit 1
    fi
    
    post="action=insert&output=text&object=item&set:service_id=9&set:hosting_order_id=${hosting_order_id}&set:dns_fqdn=${hostname}&set:dns_type=sshfp&set:dns_ttl=${dns_ttl}&set:dns_sshfp_type=2&set:dns_sshfp_algorithm=${dns_sshfp_algorithm}&set:dns_sshfp_fpr=${dns_sshfp_fpr}&user_name=$cp_username&user_pass=$cp_password&output_format=text"
    printf " Setting DNS sshfp entry for algorithm %s\n" "$dns_sshfp_algorithm"
    out=$(wget -q --post-data="$post" -O- "$url")
    if [ "$?" -ne "0" ]; then
      printf "Failed to use wget when setting the DNS entry.\n"
      exit 1
    fi
    echo "$out" | grep '"is_error":0' > /dev/null

    if [ "$?" -ne "0" ]; then
      echo "$out"
      printf "Command: wget -q -O- --post-data=%s %s\n" "$post" "$url"
      echo "Failed to create DNS record."
      exit 1
    fi
  done
fi

