#!/usr/bin/python3

# Print a report of all IP addresses, including which ones respond to pings, and the percentage
# that are in use for each block.

import os
import sys
import yaml
from IPy import IP
import subprocess
import pprint
from threading import Thread

class pingtest(Thread):
    def __init__(self, ip):
        Thread.__init__(self)
        self.ip = ip
        self.status = False 

    def run(self):
        ping_cmd = [ "ping", "-c", "1" ]
        dns_cmd = [ "dig", "+short", "-x" ]
        cmd = ping_cmd.copy()
        cmd.append(self.ip)
        result = subprocess.run(cmd, stdout=subprocess.DEVNULL)
        if result.returncode == 0:
            self.status = True 
            cmd = dns_cmd.copy()
            cmd.append(self.ip)
            # Get the first line of output.
            self.fqdn = subprocess.run(cmd, capture_output=True, text=True).stdout.strip().split("\n")[0]

def main(argv = None):
    if argv == None:
        argv = sys.argv

    # Get the path to our hosts file.
    try:
      hosts_yaml_path = argv[1]
      host_vars_path = os.path.dirname(hosts_yaml_path) + "/host_vars"

      hosts_yaml = {}
      # Load the hosts.yml file.
      with open(hosts_yaml_path) as f:
        hosts_yaml = yaml.load(f, Loader=yaml.FullLoader)
        
    except (FileNotFoundError, IndexError):
        print("Failed to load hosts or host_vars file. Please pass full path as the only argument, e.g. [command] ../inventories/dev/hosts.yml.")
        return 1 

    # Check for second argument - to limit by location.
    limit_to_location = None
    try:
        limit_to_location = argv[2]
        print("Limiting to location {0}.".format(limit_to_location))
    except (IndexError):
        # Second argument is optional
        pass


    # Create a dict of all of our networks in the format:
    # locations[location]["stats"][total] => Total IPs in this location
    # locations[location]["stats"][inuse] => Total IPs in this location that are in use
    # locations[location]["stats"][utilization] => Percent of IPs in this location that are in use 
    # locations[location]["networks"][network][total] => Total IPs in this network 
    # locations[location]["networks"][network][inuse] => count of IPs that are in use 
    # locations[location]["networks"][network][utilization] => percent of IPs that are in use 
    # locations[location]["networks"][network][usage][ip] => reverse DNS lookup of the IPs that are in use.
    # totals[total] = Total of all IPs
    # totals[inuse] => Total IPs in use
    # totals[utilization] => Percent of IPs in use 
    report = {
        "locations": {},
        "totals": {}
    }

    networks = hosts_yaml["all"]["vars"]["m_networks"]
    total = 0
    total_inuse = 0
    # Iterate over each cabinet location.
    for location in networks:
        if limit_to_location and location != limit_to_location:
            continue
        report["locations"][location] = {
            "networks": {},
            "stats": {}
        }
        print("Working on location {0}".format(location))
        location_inuse = 0
        location_total = 0
        # Iterate over each network block in this location.
        for network in networks[location]:
            # Keep track of all IPs that are in use and record the FQDN reverse DNS lookup
            usage = {}
            print(".", end="", flush=True)
            # Keep track of stats for this network.
            report["locations"][location]["networks"][network] = {}
            ips = IP(network)
            network_total = len(ips)
            report["locations"][location]["networks"][network]["total"] = network_total 
            network_inuse = 0
            broadcast = None
            gateway = None
            if network_total > 1:
                # If this is a block, then the last IP address is the broadcast
                # address which should not count and the first is the gateway
                # which should also not count.
                broadcast = ips.broadcast()
                # The second one is the gateway.
                gateway = ips[1]
                network_total = network_total - 2
            pingtests = []
            for ip in ips:
                ip = str(ip)
                p = pingtest(ip)
                pingtests.append(p)
                p.start()
            for p in pingtests:
                p.join()
                if p.ip == broadcast or p.ip == gateway:
                    # The broadcast ping test will fail but we are not including
                    # it in our total so it doesn't matter.
                    continue
                if p.status:
                    network_inuse = network_inuse + 1
                    usage[p.ip] = p.fqdn

            report["locations"][location]["networks"][network]["inuse"] = network_inuse 
            report["locations"][location]["networks"][network]["total"] = network_total
            report["locations"][location]["networks"][network]["utilization"] = "{0:.0%}".format(network_inuse / network_total)
            report["locations"][location]["networks"][network]["usage"] = usage
            location_total = location_total + network_total
            location_inuse = location_inuse + network_inuse

        report["locations"][location]["stats"] = {}
        report["locations"][location]["stats"]["total"] = location_total
        report["locations"][location]["stats"]["inuse"] = location_inuse 
        report["locations"][location]["stats"]["utilization"] = "{0:.0%}".format(location_inuse / location_total)

        total = total + location_total
        total_inuse = total_inuse + location_inuse 


    report["totals"]["total"] = total
    report["totals"]["inuse"] = total_inuse
    report["totals"]["utilization"] = "{0:.0%}".format(total_inuse / total)

    print()
    #pp = pprint.PrettyPrinter(indent=4, sort_dicts=False)
    #pp.pprint(report)
    for location in report["locations"]:
        for network in report["locations"][location]["networks"]:
            print("{0}\t{1}\t{2}\t{3}".format(
                network, 
                report["locations"][location]["networks"][network]['utilization'],
                report["locations"][location]["networks"][network]['inuse'],
                report["locations"][location]["networks"][network]['total']
            ))

    for location in report["locations"]:
        for network in report["locations"][location]["networks"]:
            for ip in report["locations"][location]["networks"][network]["usage"]:
                print("{0}\t{1}".format(ip, report["locations"][location]["networks"][network]['usage'][ip]))

if __name__ == "__main__":
    try:
        sys.exit(main())
    except ValueError as err:
        print(err)
        sys.exit(1)

