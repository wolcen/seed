import unittest
import greenhouse 
import os
import random, string
from time import sleep
import socket
import imaplib
import zmq

class GreenhouseTestCase(unittest.TestCase):
    # Our top level domain will either be dev in the dev environment or
    # test in the test environment. The main domain will always be either
    # mayfirst.test or mayfirst.dev.
    plant = None
    environment = None 
    flower_member_id = None
    flowerAdminAuthUsername = None 
    flowerAdminAuthPassword = None 
    flowerUserAuthUsername = 'testuser' 
    flowerUserAuthPassword = 'testusersecretpassword' 

    def setUp(self):
        # Setup class variables in the test class.
        self.environment = os.environ.get('GREENHOUSE_ENVIRONMENT', 'dev')
        self.flowerAdminAuthUsername = os.environ.get('GREENHOUSE_FLOWER_ADMIN_USER', 'admin')
        self.flowerAdminAuthPassword = os.environ.get('GREENHOUSE_FLOWER_ADMIN_PASSWORD', 'admin')

        # Setup the plant object we will be using through the tests.
        self.plant = greenhouse.plant()
        self.plant.emailServer = 'mail.mayfirst.' + os.environ.get('GREENHOUSE_TLD', 'dev')
        self.plant.emailBulkServer = 'bulk.mayfirst.' + os.environ.get('GREENHOUSE_TLD', 'dev')
        self.plant.flowerApiBaseUrl = os.environ.get('GREENHOUSE_FLOWER_BASE_URL', 'https://cp.mayfirst.dev/')

        self.create_membership_and_login()

    def tearDown(self):
        auth = (self.flowerAdminAuthUsername, self.flowerAdminAuthPassword)

        # Delete membership.
        if self.flower_member_id:
            self.plant.flowerApi('api/member/{0}'.format(self.flower_member_id), 'delete', {}, auth=auth)


    """
    Create a membership and login with access to the membership via the API
    using the admin user/password. This membership and login will be used
    throughout the tests.
    """
    def create_membership_and_login(self):
        auth = (self.flowerAdminAuthUsername, self.flowerAdminAuthPassword)

        # Create membership.
        member_params = {
            "name": "Test Member"
        }
        # Store the member_id - it will be used throughout the tests.
        result = self.plant.flowerApiCreate(obj="member", params=member_params, auth=auth)
        self.flower_member_id = result['id']

        # Create login.
        login_params = {
            "member": self.flower_member_id, 
            "first_name": "Testy", 
            "username": self.flowerUserAuthUsername,
            "password": self.flowerUserAuthPassword,
            "is_active": 1 
        }        
        result = self.plant.flowerApiCreate(obj='login', params=login_params, auth=auth) 
        login_id = result['id'] 

        # Grant this login access to create additional member resources.
        access_params = {
            "login": login_id,
            "member": self.flower_member_id,
            "is_active": 1
        }
        self.plant.flowerApiCreate(obj='access', params=access_params, auth=auth) 

    # Login with the provied username and password, search for the message with the given
    # subject and then ensure the provided headers are set, asserting each step of the way
    # so we can clearly see where we failed.
    def imap_login_and_find_message(self, username, password, subject = None, mailbox = None, headers = {}):
        try:
            self.assertTrue(self.plant.emailImapLogin(username, password), "Log in with username '{0}'.".format(username))
        except imaplib.IMAP4.error:
            self.assertTrue(False, "Logged in with username '{0}'.".format(username))
            return

        if subject:
            self.assertTrue(self.plant.emailImapSelectMailbox(mailbox), "Select mailbox '{0}'.".format(mailbox))
            self.assertTrue(self.plant.emailImapFindMessage(subject, mailbox), "Find message with subject {0} in mailbox {1}.".format(subject, mailbox))
        if headers:
            for header in headers:
                self.assertTrue(self.plant.emailImapMatchHeader(header), "Find message with headers {0} in mailbox {1}.".format(header, mailbox))
        self.plant.emailImapClose()

    """
    Test to ensure we can create a user with an email box and send ourselves an email
    message that will be properly delivered to our mailbox. This tests multiple components
    together (nsauth and nscache, ldap, postfix, dovecot, etc.)
    """
    def testSendRoundtripEmail(self):
        user = 'jose'
        first_name = 'Jose'
        username = 'jose'
        password = 'a super secret password'
        email_domain_name = 'icecream.dev'
        email_local = 'jose'
        email_local_alias = 'jose.gonzales'
        member_id = self.flower_member_id
        email_address = email_local + '@' + email_domain_name
        email_alias_address = email_local_alias + '@' + email_domain_name
        # The from address when submitting directly to the MX server.
        # Pick an address from a domain that accepts email and has no SPF record.
        mail_from_mx = 'info@mayfirst.info'

        # Create random string to use for subject we are sure we retrieve the email we sent.
        letters = string.ascii_lowercase
        email_subject = ''.join(random.choice(letters) for i in range(20))
        email_alias_subject = ''.join(random.choice(letters) for i in range(20))
        email_bulk_subject = ''.join(random.choice(letters) for i in range(20))
        email_mx_subject = ''.join(random.choice(letters) for i in range(20))

        # User the user credentials created during the setup phase.
        auth=(self.flowerUserAuthUsername, self.flowerUserAuthPassword)

        # First create the resources.
        login_params = {
            "member": member_id, 
            "first_name": first_name, 
            "username": username,
            "password": password,
            "is_active": 1 
        }        
        result = self.plant.flowerApiCreate(obj='login', params=login_params, auth=auth) 
        login_id = result['id'] 

        domain_zone_params = {
            "member": member_id,
            "domain": email_domain_name,
            "is_active": 1
        }
        # Note: domain_zone resources auto create MX records.
        self.plant.flowerApiCreate(obj="domain-zone", params=domain_zone_params, auth=auth)

        email_mailbox_params = {
            "member": member_id,
            "login": login_id,
            "domain_name": email_domain_name,
            "local": email_local,
            "is_active": 1
        }
        self.plant.flowerApiCreate(obj="email-mailbox", params=email_mailbox_params, auth=auth)

        # Send the message.        
        # Sleep to ensure the accounts are fully created.
        sleep(1)
        self.assertTrue(self.plant.emailSend(user, password, email_address, email_address, email_subject), "Send email")
        # Sleep to give time for the message to be delivered.
        sleep(2)

        # Try to find the message. And ensure it has headers indicating it has
        # been virus scanned and spam scanned and it's clean and that is has
        # been short circuited since it is sent locally..
        headers = [( "X-Virus-Scanned", "ClamAV using ClamSMTP"),('X-Spam-Status', 'No, score='), ('X-Spam-Status', 'SHORTCIRCUIT')]
        self.imap_login_and_find_message(user, password, subject = email_subject, headers = headers)

        # Now, try again, but with an email alias that points to the same address
        email_alias_params = {
            "member": member_id,
            "target": email_address,
            "domain_name": email_domain_name,
            "local": email_local_alias,
            "is_active": 1
        }
        self.plant.flowerApiCreate(obj="email-alias", params=email_alias_params, auth=auth)

        sleep(1)
        self.assertTrue(self.plant.emailSend(user, password, email_alias_address, email_address, email_alias_subject), "Send email via alias")
        sleep(2)

        # Try to download the message.
        self.imap_login_and_find_message(user, password, subject = email_alias_subject)

        # One more time, but this time relay via our bulk mail server.
        # Sleep to ensure the accounts are fully created.
        self.assertTrue(self.plant.emailSend(user, password, email_address, email_address, email_bulk_subject, server = 'bulk'), "Send email")
        # Sleep to give time for the message to be delivered.
        sleep(2)

        # Try to find the message.
        self.imap_login_and_find_message(user, password, subject = email_bulk_subject)

        # Test the MX server - the first time should get deferred
        #self.assertTrue(self.plant.emailSubmitRefuse('mailmx-001.mayfirst.dev', email_alias_address, mail_from_mx, email_mx_subject), "Postscreen refuses first submission (cache stored in)/var/lib/postfix/postscreen_cache.db)")

        # The second time accepted
        # Send the message - but this time with the GTUBE code so it gets tagged as spam.
        #body = 'XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X'
        #self.assertTrue(self.plant.emailSubmitAccept('mailmx-001.mayfirst.dev', email_alias_address, mail_from_mx, email_mx_subject, body), 'MX accepts second attempt')
        #sleep(2)

        # Try to download the message.
        #headers = [('X-Spam-Status', 'Yes, score=')]
        #self.imap_login_and_find_message(user, password, subject = email_mx_subject, headers = headers)

    def testWeb(self):
        return
        #web_url = 'http://webstore-001.mayfirst.' + self.tld + '/'
        #self.assertTrue(self.plant.webTest(web_url), "Web server test")
        #php_url = 'http://webstore-001.mayfirst.' + self.tld + '/helloworld.php'
        #self.assertTrue(self.plant.webTest(php_url), "PHP test")

    def testBruce(self):
        # Not yet working.
        return
        context = zmq.Context()
        mq_socket = context.socket(zmq.SUB)
        mq_socket.connect("tcp://localhost:5555")
        mq_socket.setsockopt_string(zmq.SUBSCRIBE, 'mail')

        # Wait 3 seconds for the client to connect.
        sleep(3)
        # Try 11 bad logins.
        self.plant.imapLoginWithBadPassword(11)
        # Wait 15 seconds for a response from the bruce-server.
        response = mq_socket.poll(timeout=1500)
        self.assertNotEqual(response, 0, "Multiple IMAP failures gets IP banned.")

