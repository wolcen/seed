#!/bin/bash

# Ensure local gitlab backup process is run

gitlab-backup create CRON=1 BACKUP=dump GZIP_RSYNCABLE=yes SKIP=tar
