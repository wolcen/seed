#!/bin/bash

# Script to install nextcloud. Pass $USER as first argument.

user="$1"

if [ -z "$user" ]; then
  printf "Please pass the user as the first argument.\n"
  exit 1
fi

base="/srv/nextcloud/$user"

# We need to ensure ansible has been run and has created the
# env file.
env="${base}/config/env"

if [ ! -f "$env" ]; then
  printf "The base env file does not exist (%s). Run ansible first.\n" "$env"
  exit 1
fi

# Avoid re-installing over an existing installation.
config="${base}/config/config.php"
if [ -f "$config" ]; then
  printf "The config.php file already exists (%s). Please remove it and try again.\n" "$config"
  exit 1
fi

# Avoid messing up an existing database.
database_host=$(grep ^DATABASE_HOST "${base}/config/env" | cut -d= -f2)
if ssh "postgres@${database_host}" "psql '$user' -c '\q' 2>&1" > /dev/null; then
  printf "The database already exists, please delete it first.\n"
  exit 1
fi

# Create database, user and pgpass file.
database_pass=$(hexdump -e '"%_p"' /dev/urandom | tr -d '$%.\n:[]"! \\*><}{)`(;|' | tr -d "'" | head -c 25)
database_port=5432

if ! ssh "postgres@${database_host}" "psql -c \"CREATE USER $user WITH PASSWORD '$database_pass'\""; then
  printf "Failed to create the postgres user.\n"
  exit 1
fi

ssh "postgres@${database_host}" "createdb -O '$user' '$user'"

pgpass="${base}/.pgpass"
touch "$pgpass"
chmod 600 "$pgpass"
printf "%s:%s:%s:%s:%s\n" "$database_host" "$database_port" "$user" "$user" "$database_pass" >> "$pgpass"
chown "$user:$user" "$pgpass"

# Get hostname from env file
hostname=$(grep ^HOSTNAME "${base}/config/env" | cut -d= -f2)

# We have to initialize the config.php file and indicate that we
# don't want to enable the app store, because it requires our
# apps directory to be writable by the user running the program,
# which is unsafe. We also include a few other settings that
# we'll need.
(
cat <<EOL
<?php
\$CONFIG = array (
  'appstoreenabled' => false,
  'trusted_domains' => 
  array (
    0 => '${hostname}',
  ),
  'overwrite.cli.url' => 'https://${hostname}',
  'memcache.distributed' => '\OC\Memcache\Redis',
  'redis' => [
     'host' => "${user}-redis",
     'port' => 6379,
  ],
  'memcache.locking' => '\OC\Memcache\Redis',
  'memcache.local' => '\OC\Memcache\APCu',
  'mail_smtpmode' => 'smtp',
  'mail_smtphost' => 'bulk.mayfirst.org',
  'mail_from_address' => '${user}',
  'mail_domain' => 'mayfirst.org',
);
EOL
) > "$config"

chown "$user:$user" "$config"

yaml="${base}/docker-compose.yml"

if [ ! -f "$yaml" ]; then
  printf "Can't find the yaml file (%s).\n" "$yaml"
  exit 1
fi

# Start the docker containers
printf "Starting docker containers.\n"
docker-compose --file "$yaml" up -d

# Parse out the nextcloud version.
version=$(egrep -o "NEXTCLOUD_VERSION=[0-9.]*" "$yaml" | cut -d= -f2)

admin_pass=$(hexdump -e '"%_p"' /dev/urandom | tr -d '.\n:[]"! \\*><}{)`(;|' | tr -d "'" | head -c 25)

printf "Running nextcloud installation, this might take a few minutes.\n"
docker exec "${user}-nextcloud" su -s /bin/sh -l -c "php /var/www/nextcloud/${version}/occ maintenance:install --database=pgsql \
	--database-name=${user} --database-host=$database_host --database-port=$database_port --database-user=$user \
	'--database-pass=$database_pass' --admin-user=admin '--admin-pass=$admin_pass'" www-data

# For some reason after a new install, messages pop up in the Nextcloud admin to fix these things
# (at least with version 18.0.4 - maybe try without on newer versions?).
printf "Fixing database indices.\n"
mf-nextcloud-occ-wrapper "$user" db:add-missing-indices

printf "Converting to big int"
mf-nextcloud-occ-wrapper "$user" db:convert-filecache-bigint

printf "User: admin, Password: %s\n" "$admin_pass"
