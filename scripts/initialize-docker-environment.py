#!/usr/bin/python3

# This script helps build either a dev or testing environment by creating and
# instatiating docker containers on your workstation.
#
# `initialize-docker-environment.py` ensures you have a working base image, and a
# working network. Then, it creates and starts indentical docker containers
# named after each kind of host that is defined in the ansible host file.
#
# It takes one argument: the path to your inventory file.
#
# For example:
# 
# ./initialize-docker-environment.py ../inventories/dev/hosts.yml
#
# Set the environment variable SEED_FORCE=1 to force all containers to be
# rebuilt. Set SEED_APT_PROXY to use an apt proxy. Set
# SEED_FLOWER_PATH to mount a path to your local flower git repo onto
# /usr/local/share/flower of hosts that should be running flower to assist with
# development.

import os
import sys
import yaml
import subprocess

def main(argv = None):
    if argv == None:
        argv = sys.argv

    try:
      hosts_yaml_path = argv[1]
      host_vars_path = os.path.dirname(hosts_yaml_path) + "/host_vars"

      # After reading the yaml files, we should end up with two dicts, one
      # with a list of hosts...
      hosts_yaml = {}
      # ... and one with all the host vars keyed by host name.
      hosts_vars_yaml = {} 

      # Load the hosts.yml file.
      with open(hosts_yaml_path) as f:
        hosts_yaml = yaml.load(f, Loader=yaml.FullLoader)
        for host_vars_filename in os.listdir(host_vars_path):
            host_vars_yaml_path = host_vars_path + "/" + host_vars_filename
            host_name = os.path.splitext(host_vars_filename)[0]
            if os.path.isfile(host_vars_yaml_path):
                with open(host_vars_yaml_path) as f:
                    hosts_vars_yaml[host_name] = yaml.load(f, Loader=yaml.FullLoader)
    except IndexError:
        print("Please pass path to hosts file as the first argument, e.g. ../inventories/dev/hosts.yml")
        return False
    except FileNotFoundError:
        print("Failed to load hosts or host_vars file that you passed. I tried {0}.".format(hosts_yaml_path))
        return False
    try:
      with open("docker/id_rsa.pub") as file:
          pass
    except FileNotFoundError:
        print("Failed to find your id_rsa.pub file in the docker folder. Please add your ssh public key so you can ssh into each docker image.")
        return False

    builder = buildDockerEnvironment()
    if builder.init(hosts_yaml, hosts_vars_yaml):
        builder.build()

class buildDockerEnvironment:
    force = False
    cwd = None
    apt_server = None
    flower_path = None
    hosts = [] 
    apt_server = "http.us.debian.org/debian/"
    base_net = "172.18.0"
    domain_names = []

    def init(self, hosts_yaml, hosts_vars_yaml):
        self.cwd = os.getcwd()
        if self.parse_hosts(hosts_yaml, hosts_vars_yaml) == False:
            return False
        self.flower_path = os.environ.get('SEED_FLOWER_PATH') 
            
        apt_proxy = os.environ.get('SEED_APT_PROXY') 
        if apt_proxy:
            if not apt_proxy.endswith('/'):
                apt_proxy = apt_proxy + '/'
            self.apt_server = apt_proxy + self.apt_server
        if os.environ.get('SEED_FORCE') == '1':
            print("Forcing rebuild of containers.")
            self.force = True

        if not os.path.exists('docker'):
            print("I don't see the docker directory. Please run this script while in the scripts directory.")
            return False

        return True

    # Parse out the yaml hosts.yml file to create both a list of docker containers
    # to build as well as a list of domain name/IP entries that should be added to
    # /etc/hosts.
    def parse_hosts(self, hosts_yaml, hosts_vars_yaml):
        for group, group_data in hosts_yaml['all']['children'].items():
            if 'hosts' in group_data and group_data['hosts']:
                for host, host_data in group_data['hosts'].items():
                    # In the hosts.yml file we just have keys with no data, e.g.:
                    # hosts:
                    #   server001.mayfirst.dev:
                    #   server002.mayfirst.dev:
                    # So we have to copy in the vars from the hosts_vars_yaml file.
                    try:
                        host_data = hosts_vars_yaml[host]
                    except KeyError:
                        print("I can't find a yml file for the host {0}. Please create it in host_vars and ensure it ends in .yml".format(host))
                        return False
                    # Pluck out the first address and get rid of the network part.
                    ip = host_data['network'][0]['networks'][0]['address'].split('/', 1)[0]
                    # Take the hostname without the domain name.
                    name = host.split('.', 1)[0]

                    # Build a docker container with it
                    self.hosts.append((name, ip))

                    # Add hostname to /etc/hosts file
                    self.domain_names.append((host, ip))

            # Check for additional bootstrapped dns entries
            if 'vars' in group_data and group_data['vars']:
                if 'knot_bootstrap' in group_data['vars']:
                    for dns_tuple in group_data['vars']['knot_bootstrap'].items():
                        self.domain_names.append((dns_tuple[0], dns_tuple[1]))

    def build(self):
        if not self.build_os_image():
            # If we don't have a os image, can't continue.
            return False
        self.build_seed_network()
        self.build_seed_base_image()
        self.build_and_start_containers()
        self.check_etc_hosts_file()

    # Run command, return True on returncode 0 or raise error. 
    def execute(self, args, cwd = None):
        result = subprocess.run(args, cwd = cwd)
        if result.returncode != 0:
            raise RuntimeError("Failed to run command. Args: {0}".format(args))
        return True

    # Execute shell command, return true if return code is 0, false if 1, raise
    # error otherwise.
    def shell(self, command):
        result = subprocess.run(command, shell=True, stdout=subprocess.DEVNULL)
        if result.returncode == 0:
            return True
        elif result.returncode == 1:
            return False
        else:
            raise RuntimeError("Failed to run shell command. Error: {0}".format(result.stderr))

    def network_exists(self):
        return self.shell("docker network ls | grep seed")
        
    def image_exists(self, image):
        return self.shell("docker images {0} | grep {0}".format(image))

    def container_is_running(self, container):
        return self.shell("docker ps | grep '{0}$'".format(container))

    def container_is_built(self, container):
        return self.shell("docker ps -a | grep '{0}$'".format(container))

    def build_seed_network(self):
        if self.network_exists():
            print("The network already exists.")
            return True
        # We should pass --ipv6 but that seems broken:
        # https://github.com/moby/moby/issues/28055
        return self.execute(["docker", "network", "create", "--subnet={0}.0/16".format(self.base_net), "seed"])

    def build_os_image(self):
        if self.image_exists("my-buster"):
            print("OS image already built.")
            return True

        print("Please run the ./build-os-image script in this directory as root to build your base image.")
        return False

    def build_seed_base_image(self):
        if not self.force and self.image_exists("seed-base"):
            print("The seed-base image already exists.")
            return True
        path = self.cwd + "/docker"
        return self.execute(["docker", "build", "-t", "seed-base" , "./"], cwd=path)
        
    def build_container(self, name, ip):
        if self.force:
            if self.container_is_running(name):
                result = subprocess.run(["docker", "stop", name])
            if self.container_is_built(name):
                result = subprocess.run(["docker", "rm", name])
        if self.container_is_built(name):
            print("The {0} container is already built".format(name))
            return True
        print("Building container {0}".format(name))
        args = [
            "docker",
            "create",
            "--net",
            "seed",
            "--ip",
            ip,
            "-v",
            # FIXME - We should be mounting /sys/fs/cgroups in read-only mode and we should not
            # be passing --cgroups=host. These two changes are a work-around for what seems to be
            # a bug in systemd's use of cgroups v2.
            # see https://serverfault.com/questions/1053187/docker-container-does-not-work-without-cgroupns-host-with-cgroup-v2
            # Also, does this work with earlier versions of systemd that do not default to cgroups v2?
            #"-v",
            #"/sys/fs/cgroup/systemd:/sys/fs/cgroup/systemd:rw",
            "/sys/fs/cgroup:/sys/fs/cgroup:rw",
            "--cgroupns",
            "host",
            "-v",
            self.cwd + "/docker/resolv.conf:/etc/resolv.conf",
            "--tmpfs",
            "/run:size=100M",
            "--tmpfs", 
            "/run/lock:size=100M",
            "--tmpfs", 
            "/tmp:size=100M",
            "--hostname",
            name,
            "--name",
            name
        ]

        if self.flower_path:
            args.append("-v")
            args.append("{0}:/usr/local/share/flower".format(self.flower_path))

        args.append("seed-base")
        return self.execute(args)
        
    def start_container(self, name):
        if self.container_is_running(name):
            print("The {0} container is already running".format(name))
            return True
        return self.execute(["docker", "start", name])

    def build_and_start_containers(self):
        for name, ip in self.hosts:
            self.build_container(name, ip) 
            self.start_container(name)

    def check_etc_hosts_file(self):
        print()
        print("Checking /etc/hosts file for necessary additions...")
        print()
        for name, ip in self.domain_names:
            with open('/etc/hosts') as f:
                if not "{0} {1}".format(ip, name) in f.read():
                    print("{0} {1}".format(ip, name))
        print()

if __name__ == "__main__":
    sys.exit(main())

