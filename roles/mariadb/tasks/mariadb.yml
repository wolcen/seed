---
# tasks file for mysql
- name: create configuration directory
  file:
    state: directory
    path: "{{ item }}"
  with_items:
    - /etc/mysql/conf.d
    - /etc/mysql/mariadb.conf.d

# Note: don't notify MariaDB when making configuration changes. A MariaDB
# restart takes time and causes a lot of user-facing errors. We want to be able
# to push out ansible changes at all times and not worry about disrupting
# service. Changes can always be applied manually via `SET GLOBAL x=y`.
- name: add base configuration files that go on all servers.
  copy:
    dest: /etc/mysql/conf.d/
    src: conf/{{ item }}
  with_items:
    - innodb-flush-log-at-trx-commit.cnf
    - utf8.cnf
    - open-file-limits.cnf
    - ignore-db.cnf
    - skip-name-resolve.cnf 
    - tmp-disk-table-size.cnf
    
- name: add templated config files for all servers
  template:
    dest: /etc/mysql/conf.d/{{ item }}
    src: conf/{{ item }}.j2
  with_items:
    - innodb-buffer-pool-size.cnf
    - innodb-log-file-size.cnf

- name: add custom bind configuration
  template:
    dest: /etc/mysql/mariadb.conf.d/bind.cnf
    src: conf/bind.cnf.j2
  when: mariadb_bind is defined

- name: install mysql packages
  apt:
    name:
      - mariadb-server
      # bsdmainutils is needed for hexdump to create a password.
      - bsdmainutils

- name: check for /root/.my.cnf
  stat:
    path: /root/.my.cnf
  register: root_password_initialized

- name: generate root password
  shell: hexdump -e '"%_p"' /dev/urandom | tr -d '.\n:[]"! \\*><}{)`(;|=#-' | tr -d "'" | head -c 25
  register: root_password
  when: root_password_initialized.stat.exists == False

- name: set root password
  command: mysqladmin -u root password '{{  root_password.stdout }}'
  when: root_password_initialized.stat.exists == False

- name: create /root/.my.cnf file
  copy:
    dest: /root/.my.cnf
    mode: 0600
    content: "[client]\nuser=root\npassword={{ root_password.stdout }}\n"
  when: root_password_initialized.stat.exists == False

- name: replace debian configuration file with symlink to our .my.cnf file
  file:
    state: link
    src: /root/.my.cnf
    path: /etc/mysql/debian.cnf
    force: yes

- name: create temp sql file to create users
  template:
    src: create-users.sql.j2
    dest: /root/create-users.sql
    mode: 0600
  when: mariadb_create_users is defined and vault_mariadb_users is defined
  notify: mysql create users

