This files facilitate making jibri recordings available.

The jibri service leaves recordings in /tmp/recordings/RANDOMSCTRING

The `jibri-finalize-recording` is configured to run via
`/etc/jitsi/jibri/jibri.conf`. It is passed the path to the directory holding
video file that was recorded.

It copies the file to `/var/www/[randomstring]/video.mp4`.

It also copies the index.html.template to the same directory and then emails
the link to `support@progressivetech.org`.
