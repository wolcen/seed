---
# tasks file for kvm
- name: install packages needed for kvm-manager
  apt:
    name:
      - qemu-kvm
      - bridge-utils
      - genisoimage
      - lvm2
      - udev
      - fakeroot
      - xorriso
      - grub2
      - sgabios
      - wget
      - git
      - socat
      - cpio
      - python3-yaml

- name: checkout kvm-manager code
  git:
    repo: "{{ kvm_manager_repo }}"
    dest: /usr/local/src/kvm-manager
    version: "{{ kvm_manager_version }}" 
  tags:
    - kvm-manager

- name: create symlinks for binaries
  file:
    state: link
    src: /usr/local/src/kvm-manager/{{ item }}
    path: /usr/local/sbin/{{ item }}
  with_items:
    - di-maker
    - kvm-creator
    - kvm-status
    - kvm-setup
    - kvm-start
    - kvm-stop
    - kvm-teardown
    - kvm-screen

- name: create symlink for screenrc file
  file:
    state: link
    src: /usr/local/src/kvm-manager/screenrc.kvm-manager
    path: /etc/screenrc.kvm-manager

- name: create directories
  file:
    state: directory
    path: "{{ item }}"
  with_items:
    - /usr/local/share/ISOs
    - /etc/kvm-manager

- name: copy systemd files
  copy:
    remote_src: true
    src: /usr/local/src/kvm-manager/{{ item }}
    dest: /etc/systemd/system/{{ item }}
  with_items:
    - kvm@.service
    - kvm-screen@.service
  notify:
    - reload systemd

- name: copy script to manage logical volumes
  copy:
    src: mf-manage-logical-volumes
    dest: /usr/local/sbin/mf-manage-logical-volumes
    mode: 755

- name: generate initial ISO
  command: di-maker default.iso
  args:
    chdir: /usr/local/share/ISOs
  tags:
    - iso

# The next set of tasks involve looping over all the servers that have
# specified this host as their kvm host.

# Loop over the entire inventory and add each guest that has specified
# the current host as their kvm host.
- name: append each kvm guest to the guests variable 
  set_fact: 
    kvm_guests: "{{ kvm_guests + [ hostvars[item] ] }}"
  loop: "{{ groups['all'] }}"
  when: 
    - hostvars[item].kvm is defined
    - hostvars[item].kvm.host == inventory_hostname
  tags:
    - lvm
    - iso

- name: ensure /etc/kvm-manager/<guest> exists
  file:
    state: directory
    path: /etc/kvm-manager/{{ item.kvm.name }}
  loop: "{{ kvm_guests }}"
  loop_control:
    label: "{{ item.kvm.name }}"

# Create a pre-seed file for every guest we are responsible for.
- name: build out preseed file for each guest.
  template:
    src: preseed.cfg.j2
    dest: /etc/kvm-manager/{{ item.kvm.name }}/preseed.cfg
  vars:
    - guest_hostname: "{{ item.kvm.name }}"
    - guest_ipaddress: "{{ item.network[0].networks[0].address.split('/')[0] }}" 
    - guest_nameservers: "{{ mayfirst_resolv_ips[0] }}" 
    - guest_gateway: "{{ item.network[0].networks[0].gateway }}" 
    - guest_netmask: "{{ item.network[0].networks[0].netmask }}" 
  loop: "{{ kvm_guests }}"
  loop_control:
    label: "{{ item.kvm.name }}"

# kvm-creator does a number of important steps, including creating the root
# logical volume, creating a udev rule to ensure proper ownership, creates
# a username to run the kvm guest, etc.
- name: run kvm creator for each guest on this host 
  command: kvm-creator create "{{ item.kvm.name }}" "{{ item.kvm.root_disk.volume_group }}" "{{ item.kvm.root_disk.size }}" "{{ item.kvm.ram }}"
  args:
    creates: /dev/mapper/{{ item.kvm.root_disk.volume_group }}-{{ item.kvm.name }}
  loop: "{{ kvm_guests }}"
  when:
    - m_environment == "production"
  loop_control:
    label: "{{ item.kvm.name }}"
  tags:
    - kvm

# Build out a yml file of all of our logical volumes,
# both the root volumes created by kvm-manager in the step
# above, and data volumes. Then notify our script that manages
# our logical volumes, which will create and resize as necessary.
- name: create logical volumes configuration file
  template:
    src: logical-volumes.yml.j2 
    dest: /etc/kvm-manager/logical-volumes.yml
  notify: manage logical volumes
  tags:
    - lvm

# Note: kvm-creator creates an initial /etc/kvm-manager/<guest>/env
# file, but doesn't add the data disks so we rebuild it and
# ensure it is up to date.

- name: re-create kvm env file 
  template:
    src: env.j2
    dest: /etc/kvm-manager/{{ item.kvm.name }}/env
  vars:
    guest_owner: "{{ item.kvm.name }}"
    guest_name: "{{ item.kvm.name }}"
    guest_ram: "{{ item.kvm.ram }}"
    guest_smp: "{{ item.kvm.smp | default(1) }}"
    guest_root_disk_volume_group: "{{ item.kvm.root_disk.volume_group }}"
    guest_root_disk_index: "{{ item.kvm.root_disk.index }}"
    guest_root_disk_driver: "{{ item.kvm.root_disk.driver }}"
    guest_data_disks: "{{ item.kvm.data_disks }}"
  loop: "{{ kvm_guests }}"
  loop_control:
    label: "{{ item.kvm.name }}"
  tags:
    - lvm

- name: copy over late_command file
  copy:
    src: late_command
    dest: /etc/kvm-manager/late_command
    mode: 755

- name: copy over mf-initialize-server file
  copy:
    src: mf-initialize-server 
    dest: /etc/kvm-manager/mf-initialize-server
    mode: 755

- name: create installer ISO for each guest
  shell: cp -f default.iso {{ item.kvm.name }}.iso.tmp && di-maker {{ item.kvm.name }}.iso.tmp /etc/kvm-manager/{{ item.kvm.name }}/preseed.cfg /etc/kvm-manager/late_command /etc/kvm-manager/mf-initialize-server && mv {{ item.kvm.name }}.iso.tmp {{ item.kvm.name }}.iso && ln -s -f /usr/local/share/ISOs/{{ item.kvm.name }}.iso /home/{{ item.kvm.name }}/vms/{{ item.kvm.name }}/cd.iso
  args:
    chdir: /usr/local/share/ISOs
    creates: /usr/local/share/ISOs/{{ item.kvm.name }}.iso
  loop: "{{ kvm_guests }}"
  loop_control:
    label: "{{ item.kvm.name }}"
  when:
    - m_environment == "production"
  notify:
    - enable all kvm guests
    - start all kvm guests
  tags:
    - iso

- name: create .ssh directory for all kvm guest users 
  file:
    state: directory
    path: /home/{{ item.kvm.name }}/.ssh
  loop: "{{ kvm_guests }}"
  loop_control:
    label: "{{ item.kvm.name }}"
  when:
    - m_environment == "production"

- name: grant ssh access to the kvm console user for everyone with root
  template:
    src: authorized_keys.j2
    dest: /home/{{ item.kvm.name }}/.ssh/authorized_keys
  loop: "{{ kvm_guests }}"
  loop_control:
    label: "{{ item.kvm.name }}"
  when:
    - m_environment == "production"

