# Seed

Welcome to seed, May First Movement Technology's server administration tool.

Our tool is built using [Ansible](https://ansible.com/), a server configuration
automation tool that ensures a collection of servers are configured in a
uniform way.

Our ansible configuration files define which programs are installed on which
servers and how each program should be configured.

All documentation is published via our admin-docs repository. To get started
please see [Hacking Ansible](https://docs.mayfirst.org/ansible-hacking/)
